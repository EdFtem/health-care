﻿using HealthCare.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.ViewModels
{
    public class HomeViewModel
    {
        public ApplicationUser ApplicationUser { get; set; }
    }
}
