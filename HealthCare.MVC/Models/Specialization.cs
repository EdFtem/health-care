﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.MVC.Models
{
    public class Specialization
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        ICollection<ApplicationUser> Doctors { get; set; }

        public Specialization() {
            Doctors = new List<ApplicationUser>();
        }

    }
}
