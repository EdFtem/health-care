﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Models
{
    public class Booking
    {
        public int Id { get; set; }

        [ForeignKey("dbo.AspNetUsers.Id")]
        public string DoctorId { get; set; }

        [ForeignKey("dbo.AspNetUsers.Id")]
        public string PatientId { get; set; }

        [Required]
        public DateTime BookingStartingTime { get; set; }

        [Required]
        public DateTime BookingEndingTime { get; set; }

        [Required]
        public Hospital Hospital { get; set; }
    }
}
