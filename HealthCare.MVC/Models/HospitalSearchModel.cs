﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Models
{
    public class HospitalSearchModel
    {
        public int HospitalId { get; set; }

        public string Specialization { get; set; }
    }
}
