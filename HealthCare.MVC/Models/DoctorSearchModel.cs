﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Models
{
    public class DoctorSearchModel
    {
        public string DoctorId { get; set; }

        public string UserName { get; set; }
    }
}
