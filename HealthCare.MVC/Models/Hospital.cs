﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HealthCare.MVC.Models
{
    public class Hospital
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Addres { get; set; }

        [StringLength(25)]
        public string ContactPhone { get; set; }

        [Required]
        public DateTime StartingWorkTime { get; set; }

        [Required]
        public DateTime EndingWorkTime { get; set; }

        public int Rating { get; set; }

        public string Description { get; set; }

        public string ComplexImgUrl { get; set; }

        public string PhotoUrl { get; set; }

        ICollection<ApplicationUser> Doctors { get; set; } = new List<ApplicationUser>();
    }
}
