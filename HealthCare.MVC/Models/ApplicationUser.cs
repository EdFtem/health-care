﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace HealthCare.MVC.Models
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(25)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(25)]
        public string SecondName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        [StringLength(50)]
        public string HomeAddress { get; set; }

        [StringLength(50)]
        public string WorkplaceAddress { get; set; }

        public Specialization DoctorSpecialization { get; set; }

        public Hospital Hospital { get; set; }

        public string PhotoUrl { get; set; }

    }
}
