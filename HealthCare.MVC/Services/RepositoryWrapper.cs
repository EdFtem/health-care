﻿using HealthCare.MVC.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Services
{
    public class RepositoryWrapper: IRepositoryWrapper
    {
        private ApplicationDbContext _repoContext;
        private IHospitalRepository _hospital;
        private IApplicationUserRepository _applicationUser;

        public IHospitalRepository Hospital
        {
            get
            {
                if (_hospital == null)
                {
                    _hospital = new HospitalRepository(_repoContext);
                }

                return _hospital;
            }
        }

        public IApplicationUserRepository ApplicationUser
        {
            get
            {
                if (_applicationUser == null)
                {
                    _applicationUser = new ApplicationUserRepository(_repoContext);
                }

                return _applicationUser;
            }
        }

        public RepositoryWrapper(ApplicationDbContext repositoryContext)
        {
            _repoContext = repositoryContext;
        }
    }
}
