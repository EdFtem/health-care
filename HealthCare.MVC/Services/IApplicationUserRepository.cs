﻿using HealthCare.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Services
{
    public interface IApplicationUserRepository: IRepository<ApplicationUser>
    {
    }
}
