﻿using HealthCare.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Services
{
    public class HospitalSearchServices : IHospitalSearchServices
    {
        private IRepositoryWrapper _repoWrapper;

        public HospitalSearchServices(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public List<Hospital> SearchHospital(string hospitalName){
            var hospitals = new List<Hospital>();
            if (hospitalName == null)
            {
                hospitals = _repoWrapper.Hospital.FindAll().ToList();
            }
            else
            {
                hospitals = _repoWrapper.Hospital.FindByCondition(x => x.Name == hospitalName).ToList();
            }

            return hospitals;
        }
    }
}
