﻿using HealthCare.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Services
{
    public class DoctorSearchServices : IDoctorSearchServices
    {
        private IRepositoryWrapper _repoWrapper;

        public DoctorSearchServices(IRepositoryWrapper repoWrapper)
        {
            _repoWrapper = repoWrapper;
        }

        public List<ApplicationUser> SearchDoctorsByHospitalId(int hospitalId)
        {
            return _repoWrapper.ApplicationUser.FindByCondition(x => x.Hospital.Id == hospitalId).ToList();
        }

        public List<ApplicationUser> SearchDoctorsBySpecialization(string specializationName)
        {
            return _repoWrapper.ApplicationUser.FindByCondition(x => x.DoctorSpecialization.Name == specializationName).ToList();
        }
    }
}
