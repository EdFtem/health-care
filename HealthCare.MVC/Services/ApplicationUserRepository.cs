﻿using HealthCare.MVC.Data;
using HealthCare.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Services
{
    public class ApplicationUserRepository: RepositoryBase<ApplicationUser>, IApplicationUserRepository
    {
        public ApplicationUserRepository(ApplicationDbContext repositoryContext) : base(repositoryContext){
        }
    }
}
