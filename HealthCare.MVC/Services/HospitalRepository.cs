﻿using HealthCare.MVC.Data;
using HealthCare.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Services
{
    public class HospitalRepository: RepositoryBase<Hospital>, IHospitalRepository
    {
        public HospitalRepository(ApplicationDbContext repositoryContext) : base(repositoryContext)
        {
        }
    }
}
