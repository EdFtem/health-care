﻿using HealthCare.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCare.MVC.Services
{
    public interface IDoctorSearchServices
    {
        List<ApplicationUser> SearchDoctorsByHospitalId(int hospitalId);
        
        List<ApplicationUser> SearchDoctorsBySpecialization(string specializationName);
    }
}
