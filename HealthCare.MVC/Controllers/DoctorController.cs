﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthCare.MVC.Models;
using HealthCare.MVC.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HealthCare.MVC.Controllers
{
    [Authorize]
    public class BookingController : Controller
    {
        private IRepositoryWrapper _repositoryWrapper;

        public BookingController(IRepositoryWrapper repositoryWrapper)
        {
            _repositoryWrapper = repositoryWrapper;
        }

        public IActionResult Index(DoctorSearchModel doctorSearchModel)
        {
            ApplicationUser doctor = _repositoryWrapper.ApplicationUser.FindByCondition(x => x.Id == doctorSearchModel.DoctorId).FirstOrDefault();
            return View(doctor);
        }
    }
}