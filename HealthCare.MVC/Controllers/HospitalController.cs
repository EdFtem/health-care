﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HealthCare.MVC.Models;
using HealthCare.MVC.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HealthCare.MVC.Controllers
{
    [Authorize]
    public class HospitalController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private IRepositoryWrapper _repoWrapper;
        private IHospitalSearchServices _hospitalSearchServices;
        private IDoctorSearchServices _doctorSearchServices;

        public HospitalController(UserManager<ApplicationUser> userManager, 
        IRepositoryWrapper repoWrapper,
        IHospitalSearchServices hospitalSerchService,
        IDoctorSearchServices doctorSearchServices) {
            _userManager = userManager;
            _repoWrapper = repoWrapper;
            _hospitalSearchServices = hospitalSerchService;
            _doctorSearchServices = doctorSearchServices;
        }

        public IActionResult Index(string hospitalName)
        {
            List<Hospital> hospitals = _hospitalSearchServices.SearchHospital(hospitalName);
            return View(hospitals);
        }

        public IActionResult Doctors(HospitalSearchModel hospitalSearchModel)
        {
            List<ApplicationUser> doctors = _doctorSearchServices.SearchDoctorsByHospitalId(hospitalSearchModel.HospitalId);
            return View(doctors);
        }
    }
}