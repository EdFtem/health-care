﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthCare.MVC.Data.Migrations
{
    public partial class DoctorSpecialization : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DoctorSpecializationId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_DoctorSpecializationId",
                table: "AspNetUsers",
                column: "DoctorSpecializationId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Specialization_DoctorSpecializationId",
                table: "AspNetUsers",
                column: "DoctorSpecializationId",
                principalTable: "Specialization",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Specialization_DoctorSpecializationId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_DoctorSpecializationId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "DoctorSpecializationId",
                table: "AspNetUsers");
        }
    }
}
