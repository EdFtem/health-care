﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthCare.MVC.Data.Migrations
{
    public partial class HospitalPopulationScript : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO dbo.Hospital VALUES 
                                  ('Health+','Akademika Hnatyuka Str. 12a','223-28-68','20120618 10:00:00 AM','20120618 10:00:00 PM',4,'Description','https://is.gd/HXQum9','PhotoUrl')");
            migrationBuilder.Sql(@"INSERT INTO dbo.Hospital VALUES 
                                  ('Lviv Regional Clinical Hospital','18-a Valova Str.','223-47-57','20120618 07:00:00 AM','20120618 08:00:00 PM',5,'Description','https://is.gd/Qunqlp','PhotoUrl')");
            migrationBuilder.Sql(@"INSERT INTO dbo.Hospital VALUES 
                                  ('Consultative clinic','2, Dudaeva str.','223-58-71','20120618 09:00:00 AM','20120618 11:00:00 PM',3,'Description','http://tiny.cc/wc2w5y','PhotoUrl')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("TRUNCATE TABLE dbo.Hospital");
        }
    }
}
