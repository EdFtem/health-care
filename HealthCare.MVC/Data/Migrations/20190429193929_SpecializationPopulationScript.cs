﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthCare.MVC.Data.Migrations
{
    public partial class SpecializationPopulationScript : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"INSERT INTO dbo.Specialization (Name) VALUES 
            ('Allergist'),
            ('Anaesthesiologist'),
            ('Andrologist'),
            ('Cardiologist'),
            ('Cardiac Electrophysiologist'),
            ('Dermatologist'),
            ('Endocrinologist'),
            ('Epidemiologist'),
            ('Family Medicine Physician'),
            ('Gastroenterologist'),
            ('Geriatrician'),
            ('Hyperbaric Physician'),
            ('Hematologist'),
            ('Immunologist'),
            ('Intensivist'),
            ('Medical Examiner'),
            ('Medical Geneticist'),
            ('Neonatologist'),
            ('Nephrologist'),
            ('Neurologist'),
            ('Neurosurgeon'),
            ('Oncologist'),
            ('Ophthalmologist'),
            ('Parasitologist'),
            ('Radiologist'),
            ('Surgeon'),
            ('Thoracic Surgeon'),
            ('Urologist'),
            ('Veterinarian'),
            ('Chiropractor')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"TRUNCATE TABLE dbo.Specialization");
        }
    }
}
