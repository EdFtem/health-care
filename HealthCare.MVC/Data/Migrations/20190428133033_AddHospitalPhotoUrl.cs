﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HealthCare.MVC.Data.Migrations
{
    public partial class AddHospitalPhotoUrl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PhotoUrl",
                table: "Hospital",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PhotoUrl",
                table: "Hospital");
        }
    }
}
